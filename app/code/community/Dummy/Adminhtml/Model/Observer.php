<?php 
/**
 * Magento Dummys 
 * To Help You ;)
 *
 *  @author      Rafael Kisters ventura <kistters@gmail.com>
 */
class Dummy_Adminhtml_Model_Observer extends Varien_Event_Observer
{

    public function appendFieldsToCollection(Varien_Event_Observer $observer)
    {
        $name = $observer->getEvent()->getName();
        echo $name;
        switch ($name) {
            case 'catalog_product_collection_load_before':
                Dummy_Adminhtml_Model_Grid_Product::appendFieldsToCollection($observer);
                break;
                
            case 'sales_order_grid_collection_load_before':
                Dummy_Adminhtml_Model_Grid_Order::appendFieldsToCollection($observer);
                break;
        }
    }
    /**
     *
     */
    public function appendColumnToGrid(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();

        if (!isset($block)) {
            return $this;
        }

        #echo get_class($block); echo "<br>";
        switch ($block) {
            case $block instanceof Mage_Adminhtml_Block_Catalog_Product_Grid:
                Dummy_Adminhtml_Model_Grid_Product::appendColumnToGrid($block);
                break;

            case $block instanceof Mage_Adminhtml_Block_Promo_Quote_Grid:
                Dummy_Adminhtml_Model_Grid_Promotion::appendColumnToGrid($block);
                break;

            case $block instanceof Mage_Adminhtml_Block_Sales_Order_Grid:
                Dummy_Adminhtml_Model_Grid_Order::appendColumnToGrid($block);
                break;
        }
        
        return;
    }
}



 ?>