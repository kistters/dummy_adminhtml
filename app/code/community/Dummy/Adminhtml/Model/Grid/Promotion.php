<?php 
/**
 * Magento Dummys 
 * To Help You ;)
 *
 *  @author      Rafael Kisters ventura <kistters@gmail.com>
 */
class Dummy_Adminhtml_Model_Grid_Promotion
{

    protected static $_fields = array();

    static public function appendColumnToGrid(Mage_Adminhtml_Block_Promo_Quote_Grid $block)
    {
        if (!self::IsActive()) {
           return;
        }

         /* @var $block Mage_Adminhtml_Block_Promo_Quote_Grid */
        $block->addColumnAfter('Icon', array(
            'header'    => '',
            'width'     => '50',
            'sortable'  => false,
            'filter'    => false,
            'align'     => 'left',
            'index'     => 'icon',
            'frame_callback' => array(new self, 'IconsRenderer')
        ), 'is_active');

        return $block;
    }

    public function IconsRenderer($value, $row, $column, $isExport)
    {
        $cupon = ($row->getCouponType() == 2)? '<span class="grid-severity-major"><span>Coupon</span></span>' : '';

        $apply = '';
        switch ($row->getSimpleAction()) {
            case 'by_percent':
                $apply = '<span class="grid-severity-notice"><span>%</span></span>';
                break;
             case 'by_fixed':
             case 'cart_fixed':
                $apply = '<span class="grid-severity-notice"><span>$</span></span>';
                break;
             case 'buy_x_get_y':
                $apply = '<span class="grid-severity-notice"><span>1x2y</span></span>';
                break;
             case 'esmart_gift_items':
             case 'esmart_gift_cart':
                $apply = '<span class="grid-severity-minor"><span>gift</span></span>';
                break;
        }

        $stop = ($row->getStopRulesProcessing())? '<span class="grid-severity-critical"><span>Stop</span></span>': '';

        return $cupon.$apply.$stop;
    }

    static public function IsActive(){
        return Mage::getStoreConfigFlag('dummyAdmin/grid/promotion');
    }
}