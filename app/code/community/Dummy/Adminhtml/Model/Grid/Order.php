<?php 
/**
 * Magento Dummys 
 * To Help You ;)
 *
 *  @author      Rafael Kisters ventura <kistters@gmail.com>
 */
class Dummy_Adminhtml_Model_Grid_Order
{

    static public function appendFieldsToCollection(Varien_Event_Observer $observer)
    {
        if (!self::IsActive()) {
           return;
        }

        $collection = $observer->getOrderGridCollection();
        $collection->getSelect()
            ->joinLeft(array('payment' => $collection->getTable('sales/order_payment')), 'payment.parent_id=main_table.entity_id', array('payment_method'=>'method'));

    }

    static public function appendColumnToGrid(Mage_Adminhtml_Block_Sales_Order_Grid $block)
    {
        if (!self::IsActive()) {
           return;
        }

        /* @var $block Mage_Adminhtml_Block_Catalog_Product_Grid */
        $block->addColumnAfter('Payment Method', array(
            'header'    => 'Payment Method',
            'width'     => '50',
            'sortable'  => false,
            'filter'    => false,
            'align'     => 'left',
            'index' => 'payment_method',
        ), 'shipping_name');

        return $block;
    }


    static public function IsActive(){
        return true;
        #return Mage::getStoreConfigFlag('dummyAdmin/grid/product');
    }
}